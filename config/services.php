<?php
return [
    "services" => [
        "example.api"      => ["class" => \Example\Vendor\Api::class, "arguments" => ["@example.client"]],
        "example.client"   => ["class" => \Example\Vendor\Http\Client::class, "factory" => ["@example.factory", "create"]],
    ],
];