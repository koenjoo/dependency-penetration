<?php
require __DIR__."/src/DP.php";

ini_set("display_errors", true);

DP::loadResources(__DIR__."/config", ["namespaces.php", "services.json", "services.php"]);

$api = DP::get("example.api");

$client = $api->getClient();

echo $client->getUrl();