<?php
namespace Example\Vendor;

use Example\Vendor\Http\Client;

class Factory
{
    protected $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function create()
    {
        return new Client($this->url);
    }


}