<?php
namespace Example\Vendor\Http;

class Client
{
    protected $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


}