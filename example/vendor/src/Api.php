<?php
namespace Example\Vendor;

use Example\Vendor\Http\Client;

class Api
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }


}