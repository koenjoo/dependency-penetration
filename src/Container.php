<?php

class Container
{
    /** @var array */
    protected $definitions;

    /** @var array */
    protected $services;


    public function set($id, $service)
    {
        $this->services[$id] = $service;
    }

    public function addDefinition($id, array $definition)
    {
        $this->definitions[$id] = $definition;
    }

    public function has($id)
    {
        return isset($this->definitions[$id]) || isset($this->services[$id]);
    }

    public function get($id)
    {
        if(!$this->has($id)) {
            throw new \Exception("Invalid service '$id'");
        }
        if(!isset($this->services[$id])) {
            $this->instantiate($id);
        }
        return $this->services[$id];
    }

    protected function parseArgs(array $args)
    {
        $parsedArgs = [];
        foreach($args as $arg)
        {
            if(strpos($arg, "@") === 0) {
                $serviceId = substr($arg, 1);
                $parsedArgs[] = self::get($serviceId);
            } else {
                $parsedArgs[] = $arg;
            }
        }
        return $parsedArgs;
    }

    protected function instantiate($id)
    {
        $def    = $this->definitions[$id];
        $class  = $def['class'];
        $factory = isset($def['factory']) ? $def['factory'] : null;
        $args   = isset($def['arguments']) ? $def['arguments'] : [];

        DP::load($class);

        $args = $this->parseArgs($args);

        if($factory !== null) {
            $serviceId = substr($factory[0], 1);
            $factoryService = $this->get($serviceId);
            $factoryMethod = $factory[1];
            $this->services[$id] = call_user_func_array([$factoryService, $factoryMethod], $args);
        } else {
            $rc = new \ReflectionClass($class);
            $this->services[$id] = $rc->newInstanceArgs($args);
        }
    }
}