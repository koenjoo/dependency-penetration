<?php

class DP
{
    protected static $namespaces = [];

    protected static $container;

    protected static $loaded = [];

    protected static $loaders = null;

    protected static function getContainer()
    {
        if(self::$container === null) {
            require __DIR__."/Container.php";
            self::$container = new Container();
        }
        return self::$container;
    }

    public static function registerNamespace($namespace, $path)
    {
        self::$namespaces[$namespace] = $path;
    }

    public static function addDefinition($id, array $definition)
    {
        self::getContainer()->addDefinition($id, $definition);
    }

    protected static function loadPhpResource($path)
    {
        return require $path;
    }

    protected static function loadJsonResource($path)
    {
        $content = file_get_contents($path);
        return json_decode($content, true);
    }

    public static function loadResources($path, array $filenames)
    {
        foreach($filenames as $filename)
        {
            self::loadResource(rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $filename);
        }
    }

    protected static function initLoaders()
    {
        $loaders = [];
        require __DIR__."/PhpLoader.php";
        require __DIR__."/JsonLoader.php";
        $loaders[] = new PhpLoader();
        $loaders[] = new JsonLoader();
        return $loaders;
    }

    public static function getLoaders()
    {
        if(self::$loaders === null) {
            self::$loaders = self::initLoaders();
        }
        return self::$loaders;
    }

    public static function loadResource($path)
    {
        $loaders = self::getLoaders();
        $defs = null;
        foreach($loaders as $loader)
        {
            if($loader->supports($path)) {
                $defs = $loader->load($path);
                break;
            }
        }
        if($defs === null) {
            throw new \Exception("Invalid resource $path");
        }

        $namespaces = isset($defs['namespaces']) ? $defs['namespaces'] : [];
        foreach($namespaces as $namespace => $path) {
            self::registerNamespace($namespace, $path);
        }
        $services = isset($defs['services']) ? $defs['services'] : [];
        foreach($services as $id => $definition)
        {
            self::addDefinition($id, $definition);
        }
    }

    public static function get($id)
    {
        return self::getContainer()->get($id);
    }

    public static function load($class)
    {
        if(isset(self::$loaded[$class])) {
            return;
        }
        foreach(self::$namespaces as $namespace => $path)
        {
            if(strpos($class, $namespace) === 0) {
                $tail = substr($class, strlen($namespace));
                $tail = str_replace("\\", DIRECTORY_SEPARATOR, $tail);
                self::$loaded[$class] = true;
                require $path.DIRECTORY_SEPARATOR.$tail.".php";
            }
        }
    }
}