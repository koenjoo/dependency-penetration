<?php

class JsonLoader
{
    public function load($path)
    {
        $content = file_get_contents($path);
        return json_decode($content, true);
    }

    public function supports($path)
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        return $ext === "json";
    }
}