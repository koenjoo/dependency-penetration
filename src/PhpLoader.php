<?php

class PhpLoader
{
    public function load($path)
    {
        return require $path;
    }

    public function supports($path)
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        return $ext === "php";
    }
}